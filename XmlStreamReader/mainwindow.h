#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QXmlStreamReader>
#include <QHBoxLayout>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QHBoxLayout *mainLayout;
    QXmlStreamReader reader;
    QTreeWidget *tree{nullptr};
    QTreeWidgetItem* itemStart{nullptr};

    void readStartElement(QTreeWidgetItem *parent);
    void readXMLFile(QString path, QTreeWidgetItem *parent);
};
#endif // MAINWINDOW_H
