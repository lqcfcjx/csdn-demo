#include "mainwindow.h"
#include <QFile>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    mainLayout = new QHBoxLayout(this);
    tree = new QTreeWidget(this);
    tree->setStyleSheet("QTreeWidget::item{height:35px}");
    mainLayout->addWidget(tree);
    tree->clear();
    tree->setColumnCount(3);
    tree->setColumnWidth(0,300);
    tree->setColumnWidth(1,300);
    tree->setColumnWidth(2,300);
    tree->resize(900,800);
    itemStart = new QTreeWidgetItem;
    itemStart->setText(0, "click");
    tree->addTopLevelItem(itemStart);

    readXMLFile("D:/LXSY/test.xml",itemStart);
    setFixedSize(920, 800);
}

MainWindow::~MainWindow()
{
}

void MainWindow::readStartElement(QTreeWidgetItem *parent){
    while (!reader.atEnd()) {
        if(reader.isEndElement() && reader.name() == parent->text(0)){
            break;
        }
        if(reader.isStartElement() && reader.name() != ""){
            QTreeWidgetItem *item = new QTreeWidgetItem(parent);
            //显示tagName名称
            item->setText(0,reader.name().toString());
            //显示摘要
            if(reader.attributes().size()>0) item->setText(2,reader.attributes().first().name() + ":" + reader.attributes().first().value());
            reader.readNext();
            //显示字符的值
            item->setText(1,reader.text().toString());
            readStartElement(item);
        }
        reader.readNext();
    }
}

void MainWindow::readXMLFile(QString path, QTreeWidgetItem *parent){
    QFile file(path);
    if(!file.open(QFile::ReadOnly | QFile::Text)){
        return;
    }
    reader.setDevice(&file);
    reader.readNext();
    while(!reader.atEnd()){
        if(reader.isStartElement()){
            readStartElement(parent);
            reader.readNext();
        }else{
            reader.readNext();
        }
    }
    file.close();
}
